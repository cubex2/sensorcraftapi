package cubex2.sensorcraft.api;

import net.minecraft.item.ItemStack;

import javax.annotation.Nonnull;

/**
 * Allows an item to be placed in a module slot
 */
public interface IItemModule
{
    ISensorModule getModule(ItemStack stack);
}
