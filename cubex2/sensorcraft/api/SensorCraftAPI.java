package cubex2.sensorcraft.api;

import net.minecraft.item.ItemStack;

import java.util.List;
import net.minecraft.util.NonNullList;

public abstract class SensorCraftAPI
{
    /**
     * Instantiated during preInit of SensorCraft
     */
    public static SensorCraftAPI instance;

    /**
     * Returns all blocks that are blacklisted due to the given modifiers.
     *
     * @param modifiers The modifiers that may contain one or more block blacklist modifiers. You usually want to
     *                  directly pass the modifiers from {@link ISensorModule#getBlockPower}
     *                  or {@link ISensorModule#getMobilePower}
     * @return All blacklisted blocks.
     */
    public abstract NonNullList<ItemStack> getBlackListedBlocks(NonNullList<ItemStack> modifiers);

    /**
     * Returns all items that are blacklisted due to the given modifiers.
     *
     * @param modifiers The modifiers that may contain one or more item blacklist modifiers. You usually want to
     *                  directly pass the modifiers from {@link ISensorModule#getBlockPower}
     *                  or {@link ISensorModule#getMobilePower}
     * @return All blacklisted items.
     */
    public abstract NonNullList<ItemStack> getBlackListedItems(NonNullList<ItemStack> modifiers);

    /**
     * Returns the total range upgrade from the given modifiers.
     *
     * @param modifiers The modifiers that may contain one or more item blacklist modifiers. You usually want to
     *                  directly pass the modifiers from {@link ISensorModule#getBlockPower}
     *                  or {@link ISensorModule#getMobilePower}
     * @return The total range upgrade or 0 if there isn't any.
     */
    public abstract int getTotalRangeUpgrade(NonNullList<ItemStack> modifiers);
}
