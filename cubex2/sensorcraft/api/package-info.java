@API(owner = "sensorcraft", apiVersion = "3", provides = "SensorCraftAPI")
@ParametersAreNonnullByDefault
@MethodsReturnNonnullByDefault
package cubex2.sensorcraft.api;

import javax.annotation.ParametersAreNonnullByDefault;
import mcp.MethodsReturnNonnullByDefault;
import net.minecraftforge.fml.common.API;