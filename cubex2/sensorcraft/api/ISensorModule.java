package cubex2.sensorcraft.api;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.List;

public interface ISensorModule
{
    /**
     * Gets the power for the sensor block.
     *
     * @param world     The world the sensor is in
     * @param pos       The position of the sensor
     * @param stack     The stack that returned this ISensorModule
     * @param modifiers The modifiers for this module. Even though this list contains output modifiers, they shouldn't
     *                  be used here as they are applied on the result of this method.
     * @return The power this module provides. After the output modifiers have been applied, the power will be clamped
     * between 0 and 15. For on/off only modules this should return either 0 or 15.
     */
    int getBlockPower(World world, BlockPos pos, ItemStack stack, NonNullList<ItemStack> modifiers);

    /**
     * Gets the power for the mobile sensor.
     *
     * @param world     The world the player is in
     * @param player    The player that holds the sensor
     * @param stack     The stack that returned this ISensorModule
     * @param modifiers The modifiers for this module. Even though this list contains output modifiers, they shouldn't
     *                  be used here as they are applied on the result of this method.
     * @return The power this module provides. After the output modifiers have been applied, the power will be clamped
     * between 0 and 15. For on/off only modules this should return either 0 or 15.
     */
    int getMobilePower(World world, EntityPlayer player, ItemStack stack, NonNullList<ItemStack> modifiers);
}
